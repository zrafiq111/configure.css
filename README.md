# ConfigureCSS

No more editing css for a long time. <br>
This is very suitable for use as a complement to the CSS framework that you are using.

> ConfigureCSS is not a responsive framework css like `bootstrap` and etc. This is only complement css

---

## Table of Contens

- [Installation](#Installation)
- [Example](#Example)
- [Features](#Features)
- [New Update](#New-Update)
- [Next Update](#Next-Update)
- [Creator](#Creator)

---

## Installation

Import `configure.css` into your project.<br>
```html
<link rel="stylesheet" type="text/css" href="Path/to/configure.css">
```
or inside your css file
```css
@import url(Path/to/configure.css);
```
And if you don't want to download it, you can use this link.
```html
<link rel="stylesheet" type="text/css" href="https://zrafiq111.netlify.com/configure.css">
```

---

## Example

> You can add the class and add it inside the tag html you want to edit. <br>

<img src="https://raw.githubusercontent.com/zrafiq111/User-profile/master/before.png" width="75%">

```html
<h2 class=" fw-8 grey-8-font" data-animate="ts-fadeInUp">APA KATA MEREKA?</h2>
<blockquote class="blockquote mt-150px">
  <figure>
    <aside>
      <i class="fa fa-quote-right"></i>
    </aside>
   <div class="ts-circle__lg"></div>
  </figure>
  <p class=" f-center grey-5-font ">
  Morbi et nisl a sapien malesuada scelerisque. Suspendisse tempor turpis mattis nibh posuere. Aenean sagittis nisl.
  uthicula sagitti
  </p>
  <footer class="blockquote-footer">
  <h4 class=" red-7-font f-up fw-6 ">Ahmad Zahraturrafiq</h4>
  <h6 class=" red-6-font fw-5 ">SMKN 1 CIREBON</h6>
  </footer>
</blockquote>
```

<img src="https://raw.githubusercontent.com/zrafiq111/User-profile/master/AFTER.png" width="75%">

The structure:

- `fw`,`f`,`red`,`grey`  = for the category of class
- `8`,`150px`,`center`,`7`,`6`,`5` = size or sub-category of class
- `font` = sub-category-2 class

---

## Features

Tree view of `configure.css`

```
configure.css
├─── Font
|   ├── .fbreak
|   ├── .fup & .flow
|   ├── .fleft & .fright
|   ├── .fcenter
|   ├── .fbold & .flight
|   └── .f1 ~ .f100 (%,px)*
├─── Height
|   ├── .h1px ~ .h500px
|   ├── .h1 ~ .h100 (%,vh)*
|   ├── .hmin1px ~ .hmin500px
|   ├── .hmin1 ~ .hmin100 (%,vh)*
|   ├── .hmax1px ~ .hmax500px
|   └── .hmax1 ~ .hmax100 (%,vh)*
├─── Width
|   ├── .w1px ~ .w500px
|   ├── .w1 ~ .w100 (%,vh)*
|   ├── .wmin1px ~ .wmin500px
|   ├── .wmin1 ~ .wmin100 (%,vh)*
|   ├── .wmax1px ~ .wmax500px
|   └── .wmax1 ~ .wmax100 (%,vh)*
├─── Margin left
|   ├── .ml1px ~ .ml250px
|   └── .mln1px ~ .mln250px (minus value)
├─── Margin right
|   ├── .mr1px ~ .mr250px
|   └── .mrn1px ~ .mrn250px (minus value)
├─── Margin top
|   ├── .mt1px ~ .mt250px
|   └── .mtn1px ~ .mtn250px (minus value)
├─── Margin bottom
|   ├── .mb1px ~ .mb250px
|   └── .mbn1px ~ .mbn250px (minus value)
├─── Padding left
|   ├── .pl1px ~ .pl250px
|   └── .pln1px ~ .pln250px (minus value)
├─── Padding right
|   ├── .pr1px ~ .pr250px
|   └── .prn1px ~ .prn250px (minus value)
├─── Padding top
|   ├── .pt1px ~ .pt250px
|   └── .ptn1px ~ .ptn250px (minus value)
├─── Padding bottom
|   ├── .pb1px ~ .pb250px
|   └── .pbn1px ~ .pbn250px (minus value)
├─── Opacity
|   └── .o0 ~ .o1
├─── Color
|   ├── .cc*font & cc*bg
|   ├── .blackfont & .blacktext
|   ├── .whitefont & .whitebg
|   ├── .color***value****text
|   └── .color***value****bg
├─── Text
|   ├── .tdunderline
|   └── .t.dnone
└─── Other
    ├── .tl (top left)
    ├── .tr (top right)
    ├── .bl (bottom left)
    ├── .br (bottom right)
    ├── .oxhidden (overflow x hidden)
    ├── .oyhidden (overflow y hidden)
    ├── .fw1 ~ .fw10 (fontweight)
    ├── .borderround
    └── .border0

```

` * `units available 

` **`if the unit class name is empty the unit will automatically go to percent 
 
` ***` color available = `grey` , `red` , `green` , `blue`, `yellow` 

` ****` available values = 1 ~ 10 

---


## New Update

> v.1.1.5 

- +Colour configuration for background and font. 

---

## Next Update

> All updates will be there every once a week <br>

Next update:

- Line height
- Color custom
- Customizable `configure.css`

For request update just email me : <a href="mailto:achmad.zahra62@gmail.com">achmad.zahra62@gmail.com</a>

---

## Creator

| <a href="http://zet.rf.gd/" target="_blank">**Ahmad Zahraturrafiq**</a> |
| :---: |
| [![zrafiq111](https://raw.githubusercontent.com/zrafiq111/User-profile/master/photo.png)]()    |
| `Indonesia` |


Wa : <a href="https://wa.me/6281312119466?text=Hello%20Zet">+62 81312119466</a><br>
Fb : <a href="https://web.facebook.com/rfq.ns">Ahmad Zet </a>
